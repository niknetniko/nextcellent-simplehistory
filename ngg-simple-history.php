<?php
/**
 * Plugin Name:     NextCellent Simple History
 * Plugin URI:      https://bitbucket.org/niknetniko/ngg-simplehistory
 * Description:     Adds NextCellent support for Simple History.
 * Version:         2.0.1
 * Author:          niknetniko
 * Text Domain:     ngg-simple-history
 * Domain Path:     /languages
 * License:         GPL2+
 *
 * This file is part of NextCellent Simple History.
 *
 * NextCellent Simple History is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * NextCellent Simple History is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NextCellent Simple History.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
/**
 * Load support libraries
 */
if(!class_exists('Admin_Error')) {
	require_once( __DIR__ . '/lib/ngg-admin-error.php' );
}
if(!class_exists('Admin_Notice')) {
	require_once( __DIR__ . '/lib/ngg-admin-notice.php' );
}

function ngc_simple_history() {

	load_plugin_textdomain( 'ngg-simple-history', false, basename( dirname(__FILE__) ) . '/languages/' );

	//Check for PHP version
	if ( version_compare( phpversion(), "5.4", "<" ) ) {
		$notice = new Admin_Error( printf( __( 'NGG History requires PHP 5.4 or later (you have version %s).',
			'ngg-simple-history' ), phpversion() ), 'error' );
		$notice->show();

		return;
	}

	require_once('class-ncg-simple-history.php');

	$url = plugin_dir_url( __FILE__ );
	$plugin = new NCG_Simple_History($url);
	$plugin->start();
}

//The plugin needs to be activated after loading, since we sometimes need other plugins to be loaded.
add_action( 'plugins_loaded', 'ngc_simple_history' );