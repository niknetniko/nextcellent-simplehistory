<?php

class NCG_Simple_History {

	private $url;

	public function __construct( $url ) {
		$this->url = $url;
	}

	private function check_requisites() {
		if ( !is_plugin_active( 'simple-history/index.php' ) || !defined( 'NGGFOLDER' ) ) {
			$notice = new Admin_Error( __( 'NCG History requires SimpleHistory and NextCellent in order to work.', 'ngg-simple-history' ) );
			$notice->show();

			return false;
		} else {
			return true;
		}
	}

	/**
	 * Place code for your plugin's functionality here.
	 */
	public function start() {
		if ( $this->check_requisites() ) {
			add_filter( "simple_history/loggers_files", [ $this, "load_files" ] );
		}
	}

	/**
	 * Add our loggers.
	 *
	 * @access private
	 *
	 * @param $files array The already loaded files.
	 *
	 * @return array The result.
	 */
	public function load_files( $files ) {
		$files = array_merge( $files, $this->get_loggers() );

		return $files;
	}

	/**
	 * Get all our loggers.
	 *
	 * @return array The absolute paths to the loggers.
	 */
	private function get_loggers() {
		return [
			__DIR__ . "/loggers/NextCellentLogger.php",
		];
	}
}